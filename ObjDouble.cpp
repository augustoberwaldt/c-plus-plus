//
// Created by dev on 09/03/16.
//

#include "ObjDouble.h"
#include "ObjBase.h";
double ObjDouble::getValue() const {
    return value;
}

void ObjDouble::setValue(double value) {
     ObjDouble::value = value;
}

void  ObjDouble::print()
{
    cout <<  this->getName() << "-"  << this->getValue() << endl;
}