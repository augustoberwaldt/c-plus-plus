//
// Created by dev on 09/03/16.
//

#include "ObjBase.h"
#include <iostream>
using namespace std;
ObjBase& ObjBase::setName( const std::string & name)
{
    this->name = name;
    return *this;
}

const std::string &ObjBase::getName() const
{
    return this->name;
}



void  ObjBase::print()
{
    cout <<  this->getName() << endl;
}

