//
// Created by dev on 09/03/16.
//

#include "ObjInt.h"
#include "ObjBase.h"
int ObjInt::getValue() const {
    return this->value;
}

void ObjInt::setValue(int value) {
    this->value = value;
}

void  ObjInt::print()
{
    cout <<  this->getName() << "-" << this->getValue() << endl;
}
