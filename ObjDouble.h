//
// Created by dev on 09/03/16.
//

#ifndef AULA_OBJDOUBLE_H
#define AULA_OBJDOUBLE_H
#include "ObjBase.h"
#include <iostream>
using namespace std;
class ObjDouble : public  ObjBase {

private :
    double value;
    public:
    void setValue(double value);
    double getValue() const;
    void  print();


};


#endif //AULA_OBJDOUBLE_H
