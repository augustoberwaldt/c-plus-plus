#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include "ObjBase.h"
#include "ObjDouble.h"
#include "ObjInt.h"


int main()
{

      ObjBase* base[3]  ;
      base[0] = new ObjBase;
      base[1] = new ObjDouble;
      base[2] = new ObjInt();

      base[0] ->setName("base");
      base[1] ->setName("double");
      base[2] ->setName("Int");




      base[0] ->print();
      base[1] ->print();
      base[2] ->print();

    return 0;
}