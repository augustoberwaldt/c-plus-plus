//
// Created by dev on 09/03/16.
//

#ifndef AULA_OBJBASE_H
#define AULA_OBJBASE_H

#include <string>

class ObjBase {

    private :
       std::string name;

    public :
        ObjBase  & setName(const std::string & name);
        const std::string &getName() const;
        void print();



};


#endif //AULA_OBJBASE_H
