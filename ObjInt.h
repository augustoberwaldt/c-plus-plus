//
// Created by dev on 09/03/16.
//

#ifndef AULA_OBJINT_H
#define AULA_OBJINT_H
#include "ObjBase.h"
#include <iostream>
using namespace std;
class ObjInt : public ObjBase  {

private :
    int value;

public:
    void  print();
    void setValue(int value);
    int getValue() const;

};


#endif //AULA_OBJINT_H
